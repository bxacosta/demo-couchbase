# Instalar Couchbase Docker

Guia para instalar couchbase en contenedores docker.

## Crear Network

> `docker network create couchbase`

## Crear Docker

> `docker run -d --name couchbase1 --net couchbase -p 8091-8096:8091-8096 -p 11210-11211:11210-11211 couchbase`

## Configurar couchbase

En el navegador web ingresar a la url:

> `http://<docker-host>:8091`

Seleccionar la opción **Setup New Cluster**.

Ingresar los datos del **Cluster Name** (local) y el **Password** y continuar.

Marcar en aceptar los terminos y condiciones y seleccionar la opción **Finish With Defaults**.

## Crear bucket

En la barra lateral izquierda seleccionar la opción **Buckets**.

Seleccionar la opción **ADD BUCKET**.

Ingresar un nombre (test).

Seleccionar la opción **Add Bucket**.

## Crear un usuario para el bucket

En la barra lateral izquierda seleccionar la opción **Security**.

Seleccionar la opción **ADD USER**.

Ingresar los datos de **Username** (**IMPORTANTE:** este debe ser igual al nombre del bucket creado) en este caso **test** y el **Password**, en la parte derecha en la sección de **Roles** en la parte inferior se encuentra el nombre bucket creado, expandir todas las opciones asociadas a este y marcarlas.

Seleccionar la opción **Add User**.

## Modificar los datos en las propiedades del proyecto

Para poder ejecutar el proyecto es necesario cambiar los datos en el archivo de propiedades del proyecto SpringBoot a los datos configurados anteriormente.

## Probar Proyecto

Para asegurarse que todo funcione correctamente una vez ejecutado el proyecto abrir el navegador web e ingresar a la siguiente url:

> `http://<app-host>:8080/song/new`

El resultado que debe obtener es el siguiente

> ```json
> [{"id":"1","name":"musica1","gender":"genero1"},
> {"id":"2","name":"musica2","gender":"genero2"},
> {"id":"3","name":"musica3","gender":"genero1"}]
> ```

Realizar busqueda por genero:

> `http://<app-host>:8080/song/name/musica1`

Realizar busqueda por genero:

> `http://<app-host>:8080/song/gender/genero1`

# Agregar un nodo de couchbase

Para agregar un nuevo nodo es necesario crear un nuevo docker con el siguiente comando:

> `docker run -d --name couchbase2 --net couchbase couchbase`

## Ejecutar el siguinete comando para ver la ip del nuevo nodo

> `docker inspect --format '{{ .NetworkSettings.Networks.couchbase.IPAddress }}' couchbase2`

## Configurar nuevo nodo

En la misma interface web en la que se realizaron las configuraciones anteriormente, en la barra lateral izquierda en la opción **Server** seleccionar la opción **ADD SERVER**.

Ingresar los datos de **Hostname/IP Address** que es la IP obtenida al ejecutar el comando anterior en el docker,
seleccionar la opción **Add Server**.

Para finalizar la configuración del nuevo nodo seleccionar la opción **Revalance**.