package com.bxacosta.controllers;

import com.bxacosta.collections.Song;
import com.bxacosta.repositories.SongRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MainController {

    private final SongRepository songRepository;

    public MainController(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @GetMapping("/song")
    public List<Song> listSongs() {
        return songRepository.findAll();
    }

    @GetMapping("/song/new")
    public List<Song> createSongs() {

        List<Song> songs = new ArrayList<>();

        songs.add(new Song( "1", "musica1", "genero1"));
        songs.add(new Song( "2", "musica2", "genero2"));
        songs.add(new Song( "3", "musica3", "genero1"));

        for (Song song : songs) {
            songRepository.save(song);
        }

        return songs;
    }

    @GetMapping("/song/name/{name}")
    public List<Song> getByName(@PathVariable String name) {
        return songRepository.findByName(name);
    }

    @GetMapping("/song/gender/{gender}")
    public List<Song> getByGender(@PathVariable String gender) {
        return songRepository.findByGender(gender);
    }
}
