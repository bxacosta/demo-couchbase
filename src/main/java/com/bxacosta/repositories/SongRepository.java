package com.bxacosta.repositories;

import com.bxacosta.collections.Song;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "song")
public interface SongRepository extends CouchbaseRepository<Song, String> {

    @Query("#{#n1ql.selectEntity}")
    List<Song> findAll();

    @Query("#{#n1ql.selectEntity} where #{#n1ql.filter} and name = $1")
    List<Song> findByName(String name);

    List<Song> findByGender(String gender);
}
